import {NgModule} from '@angular/core';
import {RouterModule , Routes} from '@angular/router';
import { MainComponent } from './main/main.component';
import { MenuComponent } from './menu/menu.component';
import { NotFoundComponent } from './not-found/not-found/not-found.component';
import { AccessRoleGuard } from './core/guards/access-role.guard';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  {
    path: '' ,
    component: MainComponent ,
    children: [
      {
        path: '' ,
        loadChildren: './auth/auth.module#AuthModule'
      },
      {
        path: 'login' ,
        loadChildren: './auth/auth.module#AuthModule'
      },
      {
        path: 'menu' ,
        component: MenuComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'records',
        loadChildren: './records/records.module#RecordsModule',
        canLoad: [AuthGuard]
      },
      {
        path: 'users' ,
        loadChildren: './users/users.module#UsersModule',
        canLoad: [AuthGuard]
      },
      {
        path: '**' ,
        component: NotFoundComponent
      },
    ]
  } ,
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
