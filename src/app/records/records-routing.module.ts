import {NgModule} from '@angular/core';
import {RouterModule , Routes} from '@angular/router';

import { RecordsViewComponent } from './pages/records-view/records-view.component';
import { RecordAddComponent } from './pages/record-add/record-add.component';
import { RecordDetailsComponent } from './pages/record-details/record-details.component';
import { RecordSearchComponent } from './pages/record-search/record-search.component';
import { AccessRoleGuard } from '../core/guards/access-role.guard';


const routes: Routes = [
  {
    path: '' ,
    component: RecordsViewComponent,
    canActivate: [AccessRoleGuard]
  },
  {
    path: 'add' ,
    component: RecordAddComponent,
    canActivate: [AccessRoleGuard]
  },
  {
    path: 'search' ,
    component: RecordSearchComponent
  },
  {
    path: 'details/:id' ,
    component: RecordDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)] ,
  exports: [RouterModule]
})
export class RecordsRoutingModule {
}
