import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RecordsViewComponent } from './pages/records-view/records-view.component';
import { RecordDetailsComponent } from './pages/record-details/record-details.component';
import { RecordAddComponent } from './pages/record-add/record-add.component';
import { RecordsRoutingModule } from './records-routing.module';
import { RecordItemComponent } from './component/record-item/record-item.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { PaymentModalComponent } from './component/payment-modal/payment-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { RecordSearchComponent } from './pages/record-search/record-search.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    RecordsViewComponent,
    RecordDetailsComponent,
    RecordAddComponent,
    RecordItemComponent,
    PaymentModalComponent,
    RecordSearchComponent,
  ],
  imports: [
    CommonModule,
    RecordsRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatExpansionModule,
    MatDialogModule,
    MatDividerModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  entryComponents: [PaymentModalComponent],
})
export class RecordsModule { }
