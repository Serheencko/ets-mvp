import { Component, OnInit, Input, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.sass']
})
export class PaymentModalComponent implements OnInit {

  paymentValidator:boolean = true;

  constructor(
    public dialogRef: MatDialogRef<PaymentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  @Input() name;

  ngOnInit() {
   
  }
  paymentValidadion() { 
    if ((this.data.addAmount > this.data.amountToPay) || (this.data.addAmount < 0)) {
      this.paymentValidator = false;
    } else {
      this.paymentValidator = true;
    } 
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
