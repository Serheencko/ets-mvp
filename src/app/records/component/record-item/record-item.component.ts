import { Component, OnInit, Input, } from '@angular/core';
import * as  moment from 'moment';

@Component({
  selector: 'app-record-item',
  templateUrl: './record-item.component.html',
  styleUrls: ['./record-item.component.sass']
})
export class RecordItemComponent implements OnInit {

  @Input() record: any;

  public status = {
    valid: false,
    paid: false,
    invalid: false,
  }
  departureDate: String;
  returnDate: String;
  
  constructor() { }

  ngOnInit() {
    switch (this.record.status) {
      case ('valid'):
        this.status.valid = true;
        break;
      case ('paid'):
        this.status.paid = true;
        break;
      case ('invalid'):
        this.status.invalid = true;
        break;
    }
    this.departureDate = this.record.departureDate? moment(this.record.departureDate).format('L'): null;
    this.returnDate = this.record.returnDate? moment(this.record.returnDate).format('L') : null;
  }

}
