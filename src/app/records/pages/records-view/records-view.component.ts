import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/core/services/record/record.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-records-view',
  templateUrl: './records-view.component.html',
  styleUrls: ['./records-view.component.sass']
})
export class RecordsViewComponent implements OnInit {

  public records: Observable<any>
  public filtersIsOpen = true;

  filter = {
    name: null,
    id: null,
    destination: null,
  }

  constructor(private recordSrv: RecordService,) { }

  ngOnInit() {
    this.records = this.recordSrv.getRecord();
  }
  filterRecord() {
    this.records = this.recordSrv.searchRecords(this.filter);
  }
}
