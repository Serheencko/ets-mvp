import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/core/services/record/record.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-record-search',
  templateUrl: './record-search.component.html',
  styleUrls: ['./record-search.component.sass']
})
export class RecordSearchComponent implements OnInit {

  public record:any;
  recordID:string;
  constructor(private recordSrv: RecordService) { }

  ngOnInit() {
  }

  searchRecord() {
    if(this.recordID) {
      this.recordSrv.getRecordbyId(this.recordID).subscribe((res) => {
        this.record = res;        
      });
    }
  }
}
