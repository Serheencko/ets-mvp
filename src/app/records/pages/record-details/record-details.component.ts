import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PaymentModalComponent } from '../../component/payment-modal/payment-modal.component';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { RecordService } from 'src/app/core/services/record/record.service';
import { saveAs } from 'file-saver';
import * as  moment from 'moment';

@Component({
  selector: 'app-record-details',
  templateUrl: './record-details.component.html',
  styleUrls: ['./record-details.component.sass']
})
export class RecordDetailsComponent implements OnInit {
  addAmount: any;
  name: string;
  recordId: string;
  record: any;
  partialPayment = {
    yes: false,
    no: false
  };
  userRole: string;
  departureDate: String;
  returnDate: String;
  date: String;

  constructor(private modalService: NgbModal, public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private recordSrv: RecordService) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('role');
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.recordId = params.get('id');
        return this.recordSrv.getRecordbyId(this.recordId);
      })
    ).subscribe((res) => {
      this.record = res[0];
      this.departureDate = this.record.departureDate? moment(this.record.departureDate).format('L'): null;
      this.returnDate = this.record.returnDate? moment(this.record.returnDate).format('L') : null;
      this.record.payment = this.record.paymentObject.map(item => item.date = moment(item.date).format('L') + '  ' + moment(item.date).format('LT'))
      if (this.record.partialPayment) {
        this.partialPayment.yes = true;
      } else {
        this.partialPayment.no = true;
      }
    });
    
  }

  openDialog(): void {
    const data = {
      amountToPay: this.record.amountToPay,
      addAmount: 0,
      partialPayment: true,
    }
    if (!this.record.partialPayment) {
      data.addAmount = this.record.amountToPay;
      data.partialPayment = false;
    }
    const dialogRef = this.dialog.open(PaymentModalComponent, {
      width: '350px',
      height: '350px',
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addAmount = {
          "paymentAmount": result,
        }
        this.recordSrv.addPayment(this.addAmount, this.record.ID).subscribe(res => this.record = res);
      }
    });
  }
  uploadFile(file, fileName) {
    this.recordSrv.fileUpload(file).subscribe(res => saveAs(res, fileName)
    );
  }
  change(e) {
    const file = e.currentTarget.files[0];
    const formData = new FormData();
    formData.append("recordFile", file);
    this.recordSrv.addFile(this.recordId, formData).subscribe((res) => {
      this.record = res;
    });
  }
}
