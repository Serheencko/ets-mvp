import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RecordService } from 'src/app/core/services/record/record.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-record-add',
  templateUrl: './record-add.component.html',
  styleUrls: ['./record-add.component.sass']
})
export class RecordAddComponent implements OnInit {

  newRecord: FormGroup;
  error: string;
  isBtnDisabled: boolean = false;
  public products = [
    {
      id: 1,
      value: 'Flight Ticket'
    },
    {
      id: 2,
      value: 'Travel Package'
    },
    {
      id: 3,
      value: 'Rental Car'
    },
    {
      id: 4,
      value: 'Furnished Apartment'
    },
    {
      id: 5,
      value: 'Other services'
    }
  ]
  partialPayment: string = '0';
  file = [];
  maxAmountPaid:any;

  constructor(private recordSrv: RecordService, private router: Router) { }

  ngOnInit() {
    this.newRecord = new FormGroup({
      "product": new FormControl('', Validators.required),
      "firstName": new FormControl('', Validators.required),
      "lastName": new FormControl('', Validators.required),
      "departureCity": new FormControl('', Validators.required),
      "destination": new FormControl('', Validators.required),
      "departureDate": new FormControl('', Validators.required),
      "returnDate": new FormControl(''),
      "airline": new FormControl('', Validators.required),
      "amountTotal": new FormControl('', [Validators.required, Validators.min(1)]),
      "amountToPay": new FormControl('',[Validators.required, Validators.min(0)]),
      "amountAlreadyPaid": new FormControl('0', [Validators.required, Validators.min(0)]),
      "remarkPublic": new FormControl(''),
      "remarkRC": new FormControl(''),
    });
  }

  createRecord() {
    this.isBtnDisabled = true;
    this.error = '';
    const formData = new FormData();
    formData.append("product", this.newRecord.value.product);
    formData.append("firstName", this.newRecord.value.firstName);
    formData.append("lastName", this.newRecord.value.lastName);
    formData.append("departureCity", this.newRecord.value.departureCity);
    formData.append("destinationCity", this.newRecord.value.destination);
    formData.append("departureDate", this.newRecord.value.departureDate);
    formData.append("returnDate", this.newRecord.value.returnDate);
    formData.append("airline", this.newRecord.value.airline);
    formData.append("amountTotal", this.newRecord.value.amountTotal);
    formData.append("amountToPay", this.newRecord.value.amountToPay);
    formData.append("amountAlreadyPaid", this.newRecord.value.amountAlreadyPaid);
    formData.append("partialPayment", this.partialPayment);
    formData.append("remarkPublic", this.newRecord.value.remarkPublic);
    formData.append("remarkRC", this.newRecord.value.remarkRC);
    for (let i = 0; i < this.file.length; i++) {
      formData.append("recordFile", this.file[i], this.file[i]['name']);
    }
    this.recordSrv.postNewRecord(formData).subscribe(
      (data: any) => {
        this.router.navigate(['/records']);
      },
      error => {
        this.error = error.error.error.message;  
        console.log(error);
        this.isBtnDisabled = false;
      }
    );
  }

  change(e) {
    const file = e.currentTarget.files[0];
    this.file.push(file);
  }
  setPartialPayment(payment: string) {
    this.partialPayment = payment;
  }
  setAmountToPay() {
    if ((this.newRecord.value.amountTotal>=this.newRecord.value.amountAlreadyPaid) && (this.newRecord.value.amountAlreadyPaid>=0)) {
      const amountToPay = this.newRecord.value.amountTotal - this.newRecord.value.amountAlreadyPaid;
      this.newRecord.patchValue({
        amountToPay
      });
    } else {
      this.newRecord.controls['amountAlreadyPaid'].setErrors({'incorrect': true});
    }  
  }
}
