import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  user: FormGroup;
  loginError: String;

  constructor(private router: Router, private httpService: AuthService,) { }

  ngOnInit() {
    this.user = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.min(6)]),
    })
    this.user.valueChanges
    .subscribe(() => this.loginError = '');
  }

  submit() {    
    this.httpService.postLogin(this.user.value).subscribe(
      (data: any) => {      
        this.router.navigate(['/menu']);
      },
      e => {
        this.loginError = e.error.error ? e.error.error.message : 'Something went wrong';        
      }
    );   
  }
}
