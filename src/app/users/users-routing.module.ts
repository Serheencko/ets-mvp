import {NgModule} from '@angular/core';
import {RouterModule , Routes} from '@angular/router';

import { UsersViewComponent } from './pages/users-view/users-view.component';
import { UserAddComponent } from './pages/user-add/user-add.component';
import { AccessRoleGuard } from '../core/guards/access-role.guard';
import { AccessUserGuard } from '../core/guards/access-user.guard';


const routes: Routes = [
  {
    path: '' ,
    component: UsersViewComponent,
    canActivate: [AccessRoleGuard]
  },
  {
    path: 'add' ,
    component: UserAddComponent,
    canActivate: [AccessUserGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)] ,
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
