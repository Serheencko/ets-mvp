import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-view',
  templateUrl: './users-view.component.html',
  styleUrls: ['./users-view.component.sass']
})
export class UsersViewComponent implements OnInit {

  public users: Observable<any>;
  public filtersIsOpen = true;
  roles = [
    {
      id: null,
      value: ''
    },
    {
      id: 'ADMIN',
      value: 'Admin'
    },
    {
      id: 'EASE',
      value: 'EASE emplyee'
    },
    {
      id: 'PARTNER',
      value: 'Partner employee'
    }
  ]
  filter = {
    name: null,
    role: null,
    placeOfWork: null,
    city: null,
  }
  constructor(private userSrv: UserService) { }

  ngOnInit() {
    this.users = this.userSrv.getUsers();
  }
  filterUser() { 
    this.users = this.userSrv.searchUsers(this.filter); 
  }
}
