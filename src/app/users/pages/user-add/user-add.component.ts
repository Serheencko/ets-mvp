import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.sass']
})
export class UserAddComponent implements OnInit {

  newUser: FormGroup;
  error: string;
  isBtnDisabled: boolean = false;
  roles = [
    {
      id: 'ADMIN',
      value: 'Admin'
    },
    {
      id: 'EASE',
      value: 'EASE emplyee'
    },
    {
      id: 'PARTNER',
      value: 'Partner employee'
    }
  ]

  constructor(private userSrv: UserService, private router: Router) { }

  ngOnInit() {
    this.newUser = new FormGroup({
      "firstName": new FormControl('', Validators.required),
      "lastName": new FormControl('', Validators.required),
      "phoneNumber": new FormControl('', Validators.required),
      "email": new FormControl('', Validators.required),
      "role": new FormControl('', Validators.required),
      "city": new FormControl('', Validators.required),
      "placeOfWork": new FormControl('', Validators.required),
      "workPlaceID": new FormControl('', Validators.required),
      "password": new FormControl('', Validators.required),
      "passwordCheck": new FormControl('', Validators.required),
    });
  }
  createUser() {
    this.isBtnDisabled = true;
    this.error = '';
    this.userSrv.postNewUser(this.newUser.value).subscribe(
      (data: any) => {
        this.router.navigate(['/users']);
      },
      error =>{
        this.error = error.error.error.message;  
        console.log(error);
        this.isBtnDisabled = false;
      }
    );
  }
}
