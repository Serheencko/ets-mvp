import { TestBed, async, inject } from '@angular/core/testing';

import { AccessUserGuard } from './access-user.guard';

describe('AccessUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessUserGuard]
    });
  });

  it('should ...', inject([AccessUserGuard], (guard: AccessUserGuard) => {
    expect(guard).toBeTruthy();
  }));
});
