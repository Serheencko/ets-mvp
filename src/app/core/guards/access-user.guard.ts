import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccessUserGuard implements CanActivate{
  constructor(
    private router: Router,
) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean{         
    const role = localStorage.getItem('role');
    if (role === 'ADMIN') { return true; }
    this.router.navigate(['menu']);
    return false;
}
}
