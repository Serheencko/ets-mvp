import { TestBed, async, inject } from '@angular/core/testing';

import { AccessRoleGuard } from './access-role.guard';

describe('AccessRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessRoleGuard]
    });
  });

  it('should ...', inject([AccessRoleGuard], (guard: AccessRoleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
