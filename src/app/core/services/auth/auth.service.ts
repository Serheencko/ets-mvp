import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: BehaviorSubject<any> = null;
  public user$: Observable<boolean>;

  constructor() {
    const user = localStorage.getItem('token') || false;
    this.user = new BehaviorSubject(user);
    this.user$ = this.user.asObservable();
  }

  setAuth(user: boolean) {
    this.user.next(user);
}

}
