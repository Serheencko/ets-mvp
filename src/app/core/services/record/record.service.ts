import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  servUrl = environment.servUrl;

  constructor(private http: HttpClient) { }

  postNewRecord(record: any) {
    return this.http.post(`${this.servUrl}/record`, record);
  }
  getRecord() {
    return this.http.get(`${this.servUrl}/records`);
  }
  addFile(id: string, file: any) {
    return this.http.put(`${this.servUrl}/record/${id}`, file);
  }
  getRecordbyId(id:string) {
    return this.http.get(`${this.servUrl}/record/${id}`);
  }
  searchRecords(searchParams) {
    
    let params = new HttpParams();

    if (searchParams.name) {
      params = params.append('name', searchParams.name);
    }
    if (searchParams.id) {
      params = params.append('ID', searchParams.id);
    }
    if (searchParams.destination) {
      params = params.append('destination', searchParams.destination);
    }
    return this.http.get(`${this.servUrl}/records`, { params });

  }
  fileUpload(id): Observable<Blob> {
    return this.http.get(`${this.servUrl}/record/download/${id}`, {responseType: 'blob'});
  }
  addPayment(payment, recordId) {
    return this.http.post(`${this.servUrl}/payment/${recordId}`, payment);
  }
}
