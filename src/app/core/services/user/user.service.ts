import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../../../../environments/environment'

@Injectable({
  providedIn: 'root'
})

export class UserService {
  servUrl = environment.servUrl;

  constructor(private http: HttpClient) { }

  postNewUser(user: any) {
    return this.http.post(`${this.servUrl}/user`, user);
  }
  getUsers() {
    return this.http.get(`${this.servUrl}/users`);
  }

  searchUsers(searchParams) {
    
    let params = new HttpParams();

    if (searchParams.name) {
      params = params.append('name', searchParams.name);
    }
    if (searchParams.placeOfWork) {
      params = params.append('place_of_work', searchParams.placeOfWork);
    }
    if (searchParams.role) {
      params = params.append('role', searchParams.role);
    }
    if (searchParams.city) {
      params = params.append('city', searchParams.city);
    }
    return this.http.get(`${this.servUrl}/users`, { params });

  }
}
